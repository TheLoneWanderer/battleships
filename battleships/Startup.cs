using battleships.Model.Boards.BoardCreation.Builders;
using battleships.Model.Boards.BoardCreation.Factories;
using battleships.Model.Players.TargetFinders;
using battleships.Services.Battle;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "battleships", Version = "v1" });
            });

            services.AddScoped<ITargetFinder>(provider => new RandomTargetFinder());

            services.AddScoped<IShipsBoardBuilder>(provider => new ShipsBoardBuilder(10, 10));
            services.AddScoped<IBoardFactory>(provider => new BoardFactory(10, 10, provider.GetService<IShipsBoardBuilder>()));

            services.AddScoped<IBattleService>(provider => new BattleService(provider.GetService<ITargetFinder>(), provider.GetService<IBoardFactory>()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "battleships v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Ships
{
    public static class ShipTypes
    {
        public const string AircraftCarrier = "AircraftCarrier";
        public const string Battleship = "Battleship";
        public const string Destroyer = "Destroyer";
        public const string Submarine = "Submarine";
        public const string PatrolBoat = "PatrolBoat";
    }
}

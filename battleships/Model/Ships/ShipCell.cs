﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Ships
{
    public class ShipCell : IEquatable<ShipCell>
    {
        public ShipCell(int horizontalCoord, int verticalCoord)
        {
            HorizontalCoordinate = horizontalCoord;
            VerticalCoordinate = verticalCoord;
        }

        public int HorizontalCoordinate { get; }
        public int VerticalCoordinate { get; }

        public bool Equals(ShipCell otherCell)
        {
            if(otherCell != null && HorizontalCoordinate == otherCell.HorizontalCoordinate && VerticalCoordinate == otherCell.VerticalCoordinate)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return new { HorizontalCoordinate, VerticalCoordinate }.GetHashCode();
        }
    }
}

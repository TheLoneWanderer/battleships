﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Ships
{
    public class Ship
    {
        public List<ShipCell> OccupiedCells { get; set; }
        public string ShipType { get; set; }
    }
}

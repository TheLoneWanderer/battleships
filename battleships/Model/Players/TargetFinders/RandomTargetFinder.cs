﻿using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Boards.TargetsBoards;
using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Players.TargetFinders
{
    public class RandomTargetFinder : ITargetFinder
    {
        public ShipCell GetTarget(TargetsBoard targetsBoard)
        {
            var possibleTargets = targetsBoard.GetPossibleTargetsCoords();

            if(possibleTargets.Count == 0)
            {
                return null;
            }

            var randomTargetIndex = new Random().Next(0, possibleTargets.Count);
            var randomTarget = possibleTargets[randomTargetIndex];

            return randomTarget;
        }
    }
}

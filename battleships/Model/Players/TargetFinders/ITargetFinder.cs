﻿using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Boards.TargetsBoards;
using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Players.TargetFinders
{
    public interface ITargetFinder
    {
        public ShipCell GetTarget(TargetsBoard targetsBoard);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using battleships.Model.Boards.BoardCreation.Factories;
using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Boards.TargetsBoards;
using battleships.Model.Players.TargetFinders;
using battleships.Model.Ships;

namespace battleships.Model.Players
{
    public class Player
    {
        ITargetFinder targetFinder;
        TargetsBoard TargetsBoard { get; set; }

        public ShipsBoard ShipsBoard { get; }
        public string Name { get; }

        public Player(string playerName, ITargetFinder targetFinder, IBoardFactory boardFactory)
        {
            this.targetFinder = targetFinder;

            TargetsBoard = boardFactory.CreateTargetsBoard();
            ShipsBoard = boardFactory.CreateShipsBoard();

            Name = playerName;
        }

        public ShipCell FindTarget()
        {
            var target = targetFinder.GetTarget(TargetsBoard);
            return target;
        }

        public void MarkTarget(ShipCell targetCell, bool isShip)
        {
            TargetsBoard.MarkHit(targetCell.HorizontalCoordinate, targetCell.VerticalCoordinate, isShip);
        }

        public HitResponse GetHit(ShipCell targetCell)
        {
            return ShipsBoard.HitTarget(targetCell);
        }
    }
}

﻿using battleships.Model.Boards.BoardCreation.Builders;
using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Boards.TargetsBoards;
using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.BoardCreation.Factories
{
    public class BoardFactory : IBoardFactory
    {
        readonly int columns;
        readonly int rows;

        IShipsBoardBuilder shipsBoardBuilder;

        public BoardFactory(int columns, int rows, IShipsBoardBuilder shipsBoardBuilder)
        {
            this.columns = columns;
            this.rows = rows;
            this.shipsBoardBuilder = shipsBoardBuilder;
        }

        public TargetsBoard CreateTargetsBoard()
        {
            return new TargetsBoard(columns, rows);
        }

        public ShipsBoard CreateShipsBoard()
        {
            shipsBoardBuilder.AddShip(5, ShipTypes.AircraftCarrier);
            shipsBoardBuilder.AddShip(4, ShipTypes.Battleship);
            shipsBoardBuilder.AddShip(3, ShipTypes.Destroyer);
            shipsBoardBuilder.AddShip(2, ShipTypes.Submarine);
            shipsBoardBuilder.AddShip(1, ShipTypes.PatrolBoat);

            return shipsBoardBuilder.Build();
        }
    }
}

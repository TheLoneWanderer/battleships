﻿using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Boards.TargetsBoards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.BoardCreation.Factories
{
    public interface IBoardFactory
    {
        public TargetsBoard CreateTargetsBoard();
        public ShipsBoard CreateShipsBoard();
    }
}

﻿using battleships.Model.Boards.ShipsBoards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.BoardCreation.Builders
{
    public interface IShipsBoardBuilder
    {
        public void AddShip(int shipSize, string shipType);
        public ShipsBoard Build();
    }
}

﻿using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.BoardCreation.Builders
{
    public class ShipsBoardBuilder : IShipsBoardBuilder
    {
        readonly int boardColumns;
        readonly int boardRows;

        List<Ship> ships;
        List<ShipCell> boardOccupiedCells;

        public ShipsBoardBuilder(int columns, int rows)
        {
            this.boardColumns = columns;
            this.boardRows = rows;

            ships = new List<Ship>();
            boardOccupiedCells = new List<ShipCell>();
        }

        public void AddShip(int shipSize, string shipType)
        {
            while (true)
            {
                var xCoordinate = new Random(Guid.NewGuid().GetHashCode()).Next(0, boardColumns);
                var yCoordinate = new Random(Guid.NewGuid().GetHashCode()).Next(0, boardRows);
                var isHorizontal = new Random().Next(0, 2) == 0 ? true : false;

                var firstCoordinate = isHorizontal ? xCoordinate : yCoordinate;
                var lastCoordinate = firstCoordinate + (shipSize - 1);
                var boardLastCoordinate = (isHorizontal ? boardColumns : boardRows) - 1;

                if (lastCoordinate <= boardLastCoordinate)
                {
                    var shipCells = new List<ShipCell>();
                    for (int partCoordinate = firstCoordinate; partCoordinate <= lastCoordinate; partCoordinate++)
                    {
                        if(isHorizontal)
                            shipCells.Add(new ShipCell(partCoordinate, yCoordinate));
                        else
                            shipCells.Add(new ShipCell(xCoordinate, partCoordinate));
                    }

                    var overlappingCellsExist = shipCells.Intersect(boardOccupiedCells).Count() != 0;
                    if(!overlappingCellsExist)
                    {
                        boardOccupiedCells.AddRange(shipCells);
                        boardOccupiedCells.AddRange(getAdjacentCells(shipCells, isHorizontal));

                        var ship = new Ship()
                        {
                            OccupiedCells = shipCells,
                            ShipType = shipType
                        };
                        ships.Add(ship);

                        break;
                    }
                }
            }
        }

        public ShipsBoard Build()
        {
            var returnedShips = new List<Ship>(ships.GetRange(0, ships.Count));

            ships.Clear();
            boardOccupiedCells.Clear();

            var board = new ShipsBoard(returnedShips);
            return board;
        }

        private List<ShipCell> getAdjacentCells(List<ShipCell> cells, bool isHorizontal)
        {
            var potentiallyAdjacentCells = new List<ShipCell>();
            var firstCell = cells[0];
            var lastCell = cells[cells.Count - 1];

            if (isHorizontal)
            {
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate - 1, firstCell.VerticalCoordinate));
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate - 1, firstCell.VerticalCoordinate - 1));
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate - 1, firstCell.VerticalCoordinate + 1));

                if (cells.Count > 1)
                {
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate + 1, lastCell.VerticalCoordinate));
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate + 1, lastCell.VerticalCoordinate - 1));
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate + 1, lastCell.VerticalCoordinate + 1));
                }

                foreach (var cell in cells)
                {
                    potentiallyAdjacentCells.Add(new ShipCell(cell.HorizontalCoordinate, cell.VerticalCoordinate - 1));
                    potentiallyAdjacentCells.Add(new ShipCell(cell.HorizontalCoordinate, cell.VerticalCoordinate + 1));
                }
            }
            else
            {
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate, firstCell.VerticalCoordinate - 1));
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate - 1, firstCell.VerticalCoordinate - 1));
                potentiallyAdjacentCells.Add(new ShipCell(firstCell.HorizontalCoordinate + 1, firstCell.VerticalCoordinate - 1));

                if (cells.Count > 1)
                {
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate, lastCell.VerticalCoordinate + 1));
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate - 1, lastCell.VerticalCoordinate + 1));
                    potentiallyAdjacentCells.Add(new ShipCell(lastCell.HorizontalCoordinate + 1, lastCell.VerticalCoordinate + 1));
                }

                foreach (var cell in cells)
                {
                    potentiallyAdjacentCells.Add(new ShipCell(cell.HorizontalCoordinate - 1, cell.VerticalCoordinate));
                    potentiallyAdjacentCells.Add(new ShipCell(cell.HorizontalCoordinate + 1, cell.VerticalCoordinate));
                }
            }

            var adjacentCells = potentiallyAdjacentCells.Where(cell => cell.HorizontalCoordinate <= boardColumns && cell.VerticalCoordinate <= boardRows).ToList();
            return adjacentCells;
        }
    }
}

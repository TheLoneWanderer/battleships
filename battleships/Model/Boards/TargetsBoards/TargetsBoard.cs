﻿using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.TargetsBoards
{
    public class TargetsBoard
    {
        readonly int columns;
        readonly int rows;
        bool?[,] allTargets;

        public TargetsBoard(int columns, int rows)
        {
            this.columns = columns;
            this.rows = rows;
            allTargets = new bool?[this.columns, this.rows];
        }

        public void MarkHit(int targetColumn, int targetRow, bool isShip)
        {
            allTargets[targetColumn, targetRow] = isShip;
        }

        public List<ShipCell> GetPossibleTargetsCoords()
        {
            var possibleTargets = getTargetsByPredicate(target => target == null);
            return possibleTargets;
        }
        
        public List<ShipCell> GetDestroyedTargetsCoords()
        {
            var destroyedTargets = getTargetsByPredicate(target => target != null && target.Value == true);
            return destroyedTargets;
        }

        private List<ShipCell> getTargetsByPredicate(Func<bool?, bool> targetPredicate)
        {
            var specificTargets = new List<ShipCell>();

            for (int column = 0; column < columns; column++)
            {
                for (int row = 0; row < rows; row++)
                {
                    if(targetPredicate(allTargets[column, row]))
                        specificTargets.Add(new ShipCell(column, row));
                }
            }

            return specificTargets;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.ShipsBoards
{
    public class HitResponse
    {
        public bool IsShip { get; set; }
        public int ShipsRemaining { get; set; }
        public string Message { get; set; }
    }
}

﻿using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Model.Boards.ShipsBoards
{
    public class ShipsBoard
    {
        List<Ship> ships;

        public ShipsBoard(List<Ship> ships)
        {
            this.ships = ships;
        }

        public HitResponse HitTarget(ShipCell targetCell)
        {
            var hitResponse = new HitResponse();
                hitResponse.ShipsRemaining = ships.Where(ship => ship.OccupiedCells.Count() > 0).Count();

            var targetShip = ships.Where(ship => ship.OccupiedCells.Contains(targetCell)).FirstOrDefault();
            if(targetShip != null)
            {
                hitResponse.IsShip = true;
                hitResponse.Message = "Ship hit!";

                targetShip.OccupiedCells.Remove(targetCell);
                if(targetShip.OccupiedCells.Count() == 0)
                {
                    hitResponse.ShipsRemaining--;
                    hitResponse.Message = $"{targetShip.ShipType} destroyed!";
                }

                if (hitResponse.ShipsRemaining == 0)
                {
                    hitResponse.Message = $"{targetShip.ShipType} destroyed! It was the last remaining ship so the battle ends.";
                    return hitResponse;
                }

                return hitResponse;
            }
            else
            {
                hitResponse.IsShip = false;
                return hitResponse;
            }
        }
    }
}

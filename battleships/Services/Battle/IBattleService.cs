﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Services.Battle
{
    public interface IBattleService
    {
        public BattleResultDto SimulateBattle();
    }
}

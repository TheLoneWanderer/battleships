﻿using battleships.Model.Boards.ShipsBoards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Services.Battle
{
    public class BattleResultDto
    {
        public BattleResultDto()
        {
            AttacksHistory = new List<AttackResultDto>();
        }

        public BattleResultDto(ShipsBoard firstPlayerInitialBoard, ShipsBoard secondPlayerInitialBoard) : this()
        {
            FirstPlayerInitialBoard = firstPlayerInitialBoard;
            SecondPlayerInitialBoard = secondPlayerInitialBoard;
        }

        public ShipsBoard FirstPlayerInitialBoard { get; set; }
        public ShipsBoard SecondPlayerInitialBoard { get; set; }

        public List<AttackResultDto> AttacksHistory { get; set; }
    }

    public class AttackResultDto
    {
        public AttackResultDto()
        {
        }

        public AttackResultDto(string playerName, int targetColumn, int targetRow, bool isShip, string actionMessage)
        {
            CurrentPlayer = playerName;
            TargetColumn = targetColumn;
            TargetRow = targetRow;
            IsShip = isShip;
            Message = actionMessage;
        }

        public string CurrentPlayer { get; set; }

        public int TargetColumn { get; set; }
        public int TargetRow { get; set; }

        public bool IsShip { get; set; }
        public string Message { get; set; }
    }
}

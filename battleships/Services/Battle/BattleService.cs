﻿using battleships.Model.Boards.BoardCreation.Factories;
using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Players;
using battleships.Model.Players.TargetFinders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Services.Battle
{
    public class BattleService : IBattleService
    {
        ITargetFinder targetFinder;
        IBoardFactory boardFactory;

        BattleResultDto battleResults;

        public BattleService(ITargetFinder targetFinder, IBoardFactory boardFactory)
        {
            this.targetFinder = targetFinder;
            this.boardFactory = boardFactory;
        }

        public BattleResultDto SimulateBattle()
        {
            var firstPlayer = new Player("FirstPlayer", targetFinder, boardFactory);
            var secondPlayer = new Player("SecondPlayer", targetFinder, boardFactory);

            battleResults = new BattleResultDto(firstPlayer.ShipsBoard, secondPlayer.ShipsBoard);
       
            while(true)
            {
                HitResponse hitResponse;

                hitResponse = Attack(firstPlayer, secondPlayer);
                var secondPlayerShipsDestroyed = hitResponse.ShipsRemaining == 0;
                if(secondPlayerShipsDestroyed)
                {
                    break;
                }

                hitResponse = Attack(secondPlayer, firstPlayer);
                var firstPlayerShipsDestroyed = hitResponse.ShipsRemaining == 0;
                if (firstPlayerShipsDestroyed)
                {
                    break;
                }
            }

            return battleResults;
        }

        private HitResponse Attack(Player attackingPlayer, Player defendingPlayer)
        {
            while (true)
            {
                var shootTarget = attackingPlayer.FindTarget();
                var hitResponse = defendingPlayer.GetHit(shootTarget);
                attackingPlayer.MarkTarget(shootTarget, hitResponse.IsShip);

                var attackResult = new AttackResultDto(attackingPlayer.Name, shootTarget.HorizontalCoordinate, shootTarget.VerticalCoordinate, hitResponse.IsShip, hitResponse.Message);
                battleResults.AttacksHistory.Add(attackResult);

                if(!hitResponse.IsShip || hitResponse.ShipsRemaining == 0)
                {
                    return hitResponse;
                }
            }
        }
    }
}

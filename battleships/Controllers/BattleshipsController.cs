﻿using battleships.Services.Battle;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace battleships.Controllers
{
    [ApiController]
    [Route("api")]
    public class BattleshipsController : ControllerBase
    {
        IBattleService battleService;

        public BattleshipsController(IBattleService battleService)
        {
            this.battleService = battleService;
        }

        [HttpGet("battle-simulation")]
        public IActionResult GetBattleSimulation()
        {
            return Ok(battleService.SimulateBattle());
        }
    }
}

### Random ships placement ###
One of the problems I've encountered was map generation and ships placement.
By default the board has a 10x10 size, a single ship takes between 1 to 5 cells and there are usually five of them, each with different size (so basically a 1, 2, 3, 4 and 5 cell ship per board).
There should be one empty cell of space between the ships (on all the edges and both sides) and each one is placed either horizontally or vertically (they cannot be placed diagonally or form any other shape than a straight line).

So basically there are 100 cells, we need to randomly fill 15 of them, the ships cannot overlap and need to have a single cell of space around them.
I implemented the most basic solution that I could think of, which was simply generating random starting points for each ship, untill I found one that:
- does not overlap with any other ships in one of two random directions (coin-flip)
- does not overlap with any of the ships "padding" space (one cell from its sides and edges)

The case is pretty straightforward for the first ship (we start from the biggest 5-cell one, because it's simpler to have a clean board in that case, since smaller ships can fit in narrow places more easily, after the bigger ones were already placed somewhere).
The ship's cells and the space around it are then added to a list of restricted cells, which are used to validate new ship's positions.
For the next ships, we simply repeat the process of finding random starting points in a loop untill we find a place where they can fit (assuming that the ships are smaller than the first one, even though there is less and less place left - it gets easier).

It's a far from perfect solution, but assuming that (based on the game rules) we are using the default amount of ships, then in practice it's a pretty efficient and simple one.
Finally, the logic is encapsulated in a Builder pattern class, so it's possible to create a custom amount of ships, or even replace the Builder with a more sophisticated algorithm without breaking the other existing code.


### Target finding strategy ###
Another problem is finding enemy ship targets.
The game is based on the fact that there are two players and each of them has two boards (both 10x10 in size).
One of the board represents your own ships (placed randomly by yourself) - the goal of the game is to destroy all the enemy ships without knowing their position directly.
The only way to get more information about enemy ships is to choose a random position and get the response from your enemy, which should be some variant of affirmative or negative ("ship was hit" or "shot missed" without saying anything else about the ship size (unless it gets destroyed completely) or position of other ones.

The second board is used to mark targets that were already shot, either with a hit or miss (marked with a different value/color) - you can hit a specific cell only once.
It's used to predict to some degree where other ships might be located, based on their position on the map or the amount and type of already destroyed ships (the enemy should also inform the other player that a whole ship was destroyed if that's the case).

I used a straightforward aproach of simply choosing a random cell as a target, untill all the ships were destroyed by any of the player.
The algorithm is injected as a Strategy pattern into the Player class, so if in theory it was needed to modify it to a smarter one, which would be able to figure out where the other ships are by analysing the existing hits and misses, then it's should be rather trivial.


### Multiple shots ###
The game rules are based on the variation that a Player can take multiple shots untill he makes a mistake and misses.
So, in case the Player hits an enemy ship, he's allowed to take another shot.


### Returning results ###
The whole progress of the game/simulation - each player's turn and the result of hitting or missing a ship is beeing saved and after it finishes returned in a RESTful way as an API end-point.
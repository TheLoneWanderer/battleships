﻿using battleships.Model.Boards.ShipsBoards;
using battleships.Model.Ships;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace battleshipsTests.Model.Boards.ShipsBoards
{
    public class ShipsBoardUnitTests
    {
        [Fact]
        public void HitTarget_ShouldReturnTrue_WhenShipExists()
        {
            var shipCoordinates = new ShipCell(1, 1);
            var ships = new List<Ship>()
            {
                new Ship()
                {
                    OccupiedCells = new List<ShipCell>() { shipCoordinates }
                }
            };

            var shipsBoard = new ShipsBoard(ships);
            var hitResponse = shipsBoard.HitTarget(shipCoordinates.HorizontalCoordinate, shipCoordinates.VerticalCoordinate);

            Assert.True(hitResponse.IsShip);
        }

        [Fact]
        public void HitTarget_ShouldReturnFalse_WhenShipDoesNotExist()
        {
            var shipsWithoutCoords = new List<Ship>()
            {
                new Ship()
                {
                    OccupiedCells = new List<ShipCell>()
                }
            };

            var shipsBoard = new ShipsBoard(shipsWithoutCoords);
            var hitResponse = shipsBoard.HitTarget(1, 1);

            Assert.False(hitResponse.IsShip);
        }
    }
}
